﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Interfaces;
using WebApi.Models;

namespace WebApi.DAL
{
   public class BaseContext : DbContext, IBaseContext
   {
      public DbSet<Customer> Customers { get; set; }

      public async Task SaveChangesAsync()
      {
         await base.SaveChangesAsync();
      }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = "Customers.db" };
         var connectionString = connectionStringBuilder.ToString();
         var connection = new SqliteConnection(connectionString);

         optionsBuilder.UseSqlite(connection);
      }

   }
}
