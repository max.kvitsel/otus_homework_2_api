using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.DAL;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly BaseContext _context;

        public CustomerController(BaseContext context)
        {
            _context = context;
        }

        [HttpGet("{id:long}")]   
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
           var item = await _context.Customers.FirstOrDefaultAsync(z => z.Id == id);

           if (item == null)
              return NotFound();
           return Ok(item);
        }

      [HttpPost("add")]   
        public async Task<IActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (ModelState.IsValid)
            {
                await _context.Customers.AddAsync(customer);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetCustomerAsync", new { customer.Id }, customer);
            }

            return new JsonResult("Somethign Went wrong") { StatusCode = 500 };
        }
    }
}