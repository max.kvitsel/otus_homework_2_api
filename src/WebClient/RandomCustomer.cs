﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    static class RandomCustomer
    {
        public static string GetRandomName()
        {
            string[] firstNames = { "Leonard", "Isiah", "Sergio", "Bradley", "Haiden", "Deven" };

            Random random = new Random();
            string randomFirstName = firstNames[random.Next(0, 5)];
            return randomFirstName;
        }

        public static string GetRandomLastName()
        {
            string[] lastNames = { "Frey", "Houston", "Duke", "Hooper", "Mathis", "Walls" };

            Random random = new Random();
            string randomLastName = lastNames[random.Next(0, 5)];
            return randomLastName;
        }

    }
}
