﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.Json;

namespace WebClient
{
    class RunOption
    {
        static HttpClient client = new HttpClient();
        long id;
        public async Task PromptAsync()
        {
            while (true)
            {
                Console.WriteLine($"\n Options:\n 1. Request customer\n 2. Add random customer\n 3. Exit application\n\n Please choose option and press \"Enter\" [1/2/3]:");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Enter customer id:");
                        Int64.TryParse(Console.ReadLine(), out id);
                        await GetCustomer(id);
                        break;
                    case "2":
                        var customer = new Customer { Firstname = RandomCustomer.GetRandomName(), Lastname = RandomCustomer.GetRandomLastName() };
                        await CreateCustomer(customer);
                        break;
                    case "3":
                        Environment.Exit(-1);
                        break;
                    default:
                        Console.WriteLine("Invalid input. Try again.");
                        break;
                }
            }
        }

        private static async Task GetCustomer(long id)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            string url = String.Concat("https://localhost:5001/customers/", id);

            var stringTask = client.GetStringAsync(url);

            var msg = await stringTask;
            var customer = JsonSerializer.Deserialize<Customer>(msg);

            Console.WriteLine($"ID: {customer.Id}, First Name: {customer.Firstname}, Last Name: {customer.Lastname}");
        }

        private static async Task CreateCustomer(Customer customer)
        {
            client.DefaultRequestHeaders.Accept.Clear();

            string url = String.Concat("https://localhost:5001/customers/add");

            var response = await client.PostAsJsonAsync<Customer>(url, customer);

            response.EnsureSuccessStatusCode();

            var res = JsonSerializer.Deserialize<Customer>(await response.Content.ReadAsStringAsync());

            await GetCustomer(res.Id);

        }
    }
}
